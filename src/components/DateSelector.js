import React from 'react'
import SelectDate from './SelectDate'

export default class DateSelector extends React.Component{
    render(){
        return(
            <div className = 'DateSelector'>
                <SelectDate/>
                <p>to</p>
                <SelectDate/>
            </div>
        )
    }
}