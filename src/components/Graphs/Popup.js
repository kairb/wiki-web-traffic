import React from 'react';
import './style.css';
import CustomPieChart from './CustomPieChart';
import CustomBarChart from './CustomBarChart';
import CustomLineChart from './CustomLineChart';

export default class Popup extends React.Component {
	constructor(props){
        super(props)
        this.state = {showPopup: false};
	}

	render(){
		return (
			<div className='popup'>
				<div className='popup_inner'>
					<h1>{this.props.text}</h1>
        	  
					{this.props.type === 'pie'? <CustomPieChart data = {this.props.data} child={true} title='Total predicted web traffic by country' /> : null}
					{this.props.type === 'bar'? <CustomBarChart data = {this.props.data} child={true} title='Top pages by views (predicted)' /> : null}
					{this.props.type === 'line'? <CustomLineChart data = {this.props.data} child={true} title='Total predicted Web Traffic' /> : null}
				</div>
			</div>
		);
	}
}