import React, { PureComponent } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import '../../styles/GraphStyling.css'
import Popup from './Popup'

export default class CustomBarChart extends PureComponent {
  constructor(props){
    super(props)
            this.state = {showPopup: false};

}

   togglePopup() {
            this.setState({
              showPopup: !this.state.showPopup
            });
  }

  render() {
    return (
      <div  className = 'BarChart' onClick = {this.togglePopup.bind(this)}>
        <h3>{this.props.title}</h3>
          <ResponsiveContainer>
            <BarChart 
              data={this.props.data}
              margin={{
              top: 5, bottom: 10,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="Page" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="Predictions" fill="#8884d8" />
            </BarChart>
          </ResponsiveContainer>


          {this.state.showPopup ?
                      <Popup type = 'bar' data={this.props.data} closePopup={this.togglePopup.bind(this)}/>
                      : null
                   }
      </div>
    );
  }
}
