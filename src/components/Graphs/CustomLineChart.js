import React from 'react'
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,ResponsiveContainer} from 'recharts'
import '../../styles/GraphStyling.css'
import Popup from './Popup'

export default class CustomLineChart extends React.Component{
    constructor(props){
        super(props)
        this.state = {showPopup: false};
    }

     togglePopup() {
                this.setState({
                  showPopup: !this.state.showPopup
                });
              }

    render(){
        return(
            <div className = 'LineChart' onClick = {this.togglePopup.bind(this)}>
                <h3>{this.props.title}</h3>
                <ResponsiveContainer>
                    <LineChart  data = {this.props.data}
                        margin={{top: 5, right: 30, left: 30, bottom: 5}}>
                        <XAxis dataKey="Date"/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        <Line type="monotone"  dataKey="Predictions" stroke="#8884d8" activeDot={{r: 8}}/>
                    </LineChart>
                </ResponsiveContainer>

                  {this.state.showPopup ?
                     <Popup type = 'line' data={this.props.data} closePopup={this.togglePopup.bind(this)}/>: null
                  }
            </div>
        )
    }
}