import React from 'react'
import {
    PieChart, Pie, Tooltip, Legend,ResponsiveContainer, Cell} from 'recharts';
import listOfHexs from '../../other/ColorGenerater'
import '../../styles/GraphStyling.css'
import Popup from './Popup'
   
const data = [
    {Language :'zh', Predictions: 26265762},
    {Language :'fr', Predictions: 40659521},
    {Language :'en', Predictions: 54411395},
    {Language :'other', Predictions: 442008},
    {Language :'ru', Predictions: 15736331},
    {Language :'de', Predictions: 14615426},
    {Language :'ja', Predictions: 31397031},
    {Language :'es', Predictions: 1587809}
]
        
export default class CustomPieChart extends React.Component{
    constructor(props){
        super(props)
        this.state = {showPopup: false};
    }
    
    data = this.props.data

    parseData(){
        if(!this.props.data == null){
            console.log('running')
            for (var row in this.data){
                var value = this.data[row]
                this.data[row] = parseInt(value)
            }
        }

        
    }
    colors = listOfHexs(30)


     togglePopup() {
        this.setState({
            showPopup: !this.state.showPopup
                });
     }

    
    
    render(){
        return(
            <div className = 'PieChart' onClick = {this.togglePopup.bind(this)}>
                <h3>{this.props.title}</h3>
                <ResponsiveContainer>
                    <PieChart margin={{top: 5, bottom: 20}} >
                        <Pie data={data} dataKey="Predictions" 
                        nameKey="Language" cx="50%" cy="50%" outerRadius={100} label >
                            {   data == null? null: 
                                data.map((entry, index) => (
                                        <Cell key={`cell-${index}`} fill={this.colors[index]}/>
                                ))                         
                            }
                        </Pie>
                        <Tooltip/>
                        <Legend/>
                    </PieChart>
                </ResponsiveContainer>

                    {this.state.showPopup ?
                        <Popup type = 'pie' data={this.state.total} closePopup={this.togglePopup.bind(this)}/>
                        : null
                     }
            </div>
        )
    }
}