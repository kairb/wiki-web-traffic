import React from "react";
import { Input } from "reactstrap";
import '../App.css';


export default function ViewByChart() {

	const optionsChart = [
		    { Type_Of_Chart: "0" },
		    { PieChart: "1" },
		    { LineChart : "2" },
		    { BarChart: "3" },
		    { ScatterChart: "4" }
	]
		
  return (
	<div>
		<br></br>
		<div className="dropdown">
		<Input type="select">
			{optionsChart.map(option => {
			return (
				<option value={Object.values(option)}>
				{" "}
				{Object.keys(option)}{" "}
				</option>
			);
			})}
		</Input>
		</div>
	</div>
  );
}
