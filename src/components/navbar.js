import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'reactstrap';
import '../styles/navbar.css'

export default class Navbar extends Component {

  render() {
    return (
    <div className = 'pin'>
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
      <div className="align-left">
        <Link to="/" className="navbar-brand title">Wikipedia Web Traffic Predictions</Link>
      </div>
        <div className="collpase navbar-collapse">
          <div className="alignBar align-right">  
            <ul className="navbar-nav mr-auto">
                <li className="navbar-item">
                  <Link to="/" className="nav-link">Overview</Link>
                </li>
                <li className="navbar-item">
                  <Link to="/query" className="nav-link">Query</Link>
                </li>
                <li className="navbar-item">
                  <span><Button className='button' variant="primary" block> Download</Button></span>
                </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
    );
  }
}