import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


export default class SelectDate extends Component {
  constructor(props) {
    super(props);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.state = {
      date: new Date(),
    }
  }

  onChangeDate(date) {
    this.setState({
      date: date
    })
  }

  onSubmit(e) {
    e.preventDefault();
    const exercise = {date: this.state.date}
    console.log(exercise);
  }

  render() {
    return (
    <div> 
      <form onSubmit={this.onSubmit}>
        <div className="form-group">
          <div className = 'SelectDate'>
            <DatePicker
              selected={this.state.date}
              onChange={this.onChangeDate}
              dateFormat="dd/MM/yyyy"

            />
          </div>
        </div>
      </form>
    </div>
    )
  }
}