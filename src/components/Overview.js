import React from 'react'
import CustomLineChart from './Graphs/CustomLineChart'
import CustomPieChart from './Graphs/CustomPieChart'
import CustomBarChart from './Graphs/CustomBarChart'
import '../styles/GraphStyling.css'
import Pdf from "react-to-pdf";
import DateSelector from './DateSelector.js';

const ref = React.createRef();
const line= '../../data/total_views_by_date.csv'
const pie= '../../data/total_views_by_country.csv'
const bar= '../../data/total_views_by_page.csv'
const csv = require('csvtojson')

export default class Overview extends React.Component {
    state = {line: [] ,pie: [{}] ,bar: [] }

    componentDidMount() {
        this.getData(line,'line')
        this.getData(pie,'pie')
        this.getData(bar, 'bar')
    }

    getData(path,newState){
        fetch(path)
        .then(response => response.text())
        .then(csvText => csv().fromString(csvText))
        .then((data) => {
            switch(newState){
                case 'bar':
                    this.setState({bar: data });
                    break;
                case 'line':
                    this.setState({line: data });
                    break;
                case 'pie':
                    this.setState({pie: data });
                    break; 
                default:
                    console.log('error')
                }
            }
        )
    }

    render() {
        return (
            <div className='App' >
                
                <Pdf targetRef={ref} filename="code-example.pdf">
                                    {({ toPdf }) => <button onClick={toPdf}>Generate Pdf</button>}
                </Pdf>
                <DateSelector />
                <div className='align2' ref={ref}>
                    <div className='GraphContainer'>
                         <CustomPieChart title='Total web traffic by country (predicted)' data={this.state.pie}/>
                    </div>
                    <div className='GraphContainer' >
                          <CustomBarChart title='Top 10 pages by views (predicted)' data={this.state.bar} />
                    </div>
                </div>
                <div className='GraphContainer'>
                    <CustomLineChart title='Total Web Traffic (predicted)' data={this.state.line} />
                </div>
            </div>
        )
    }
}
