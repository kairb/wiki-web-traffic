import React from "react";
import { Input } from "reactstrap";
import '../App.css';


export default function ViewBy() {

  const optionsView = [
	{ View_By: "0" },
    { Year: "1" },
    { Month: "2" },
    { Day: "3" },
    { Region: "4" }
  ];
  return (
	<div>
		<br></br>
		<div className="dropdown">
		<Input type="select">
			{optionsView.map(option => {
			return (
				<option value={Object.values(option)}>
				{" "}
				{Object.keys(option)}{" "}
				</option>
			);
			})}
		</Input>
		</div>
	</div>
  );
}
