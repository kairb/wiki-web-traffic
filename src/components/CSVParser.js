import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import CSVReader from 'react-csv-reader'

export default class CSVParser extends Component {

	const csv = require('csv-parser')
	const fs = require('fs')
	const results = [];
	 
	fs.createReadStream('data.csv')
	  .pipe(csv())
	  .on('data', (data) => results.push(data))
	  .on('end', () => {
		console.log(results);
		// [
		//   { NAME: 'Daffy Duck', AGE: '24' },
		//   { NAME: 'Bugs Bunny', AGE: '22' }
		// ]
	  });  
	  
  render() {
    return (
      <CSVReader
        cssClass="csv-reader-input"
        onFileLoaded={this.handleForce}
        onError={this.handleDarkSideForce}
        inputId="ObiWan"
        inputStyle={{color: 'red'}}
      />
    )
  }
}