import React from "react";
import '../App.css';
import DateSelector from './DateSelector.js';
import ViewByChart from './ViewByChart.js';
import ViewBy from './ViewBy.js';

export default class Query extends React.Component{

	constructor() {
        super()
        this.state = {
            components: [<DateSelector />, <ViewBy />,  <ViewByChart />]
		}
		this.handleClick = this.handleClick.bind(this)
    }
    
    handleClick() {
		let temp = this.state.components
		temp.push(<ViewByChart/>)
		this.setState(temp)
    }

    render(){
		return (
			<div className = 'Container'>
				<button onClick={this.handleClick}>+</button>
				<div>
					{this.state.components.map(function(name, index){
						return <ul key={ index }>{name}</ul>;
					})}
				</div>
			</div>
  		);
	}
}
