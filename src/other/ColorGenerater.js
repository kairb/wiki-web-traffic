//generates colors array to be used in charts
export default function listOfHexs(amount){
    var colors = []
    for (let i = 0; i < amount; i++){
        colors.push(getRandomColor())
    }
    return colors
}

//generates random color
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
