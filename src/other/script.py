import csv
import random, string
from random import randint
from numpy import ndarray

NUM_OF_PAGES = 200
NUM_OF_DAYS = 30
COUNTRIES = ["EN", "FR", "ZH", "ES", "RU", "DE", "DA"]


def randomword(length):
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(length))

writer=csv.writer(open('../../public/data/forecasts.csv', 'w'), delimiter=',')
writer.writerow(['ID', 'Page_Title', 'Language', 'Date', 'Prediction'])

count = 0
for i in range(0, NUM_OF_PAGES):
    Temp_page_name = randomword(10)
    for country in COUNTRIES:
            for day in range(1,NUM_OF_DAYS+1)
                #csv.writer(count,Temp_page_name, country, day, randint(0, 1000))
                writer.writerow([count, Temp_page_name, country, day, randint(0, 1000)])
                count+=1
