const fs = require('fs')
const csv = require('csvtojson')
const final_data = require('../../data/final_data.csv')


const createCsvWriter = require('csv-writer').createObjectCsvWriter
/*const csvWriter = createCsvWriter({
    header: ['Date', 'Date_Prediction'],
    path: '../../data/final_data.csv'
});*/

const sevenDayForcast = '../data/seven_day_forecasted_visits.csv'

function formatChartData(path){
    var string = fs.readFileSync(path).toString()
    var temp = csv().fromString(string).then(csv => {
        calculateTotals(csv)
    })
}

function calculateTotals(csv){
    var totalViews = {}
    var pageViews = {}
    var countryViews = {}
    for(row in csv){
        //overall traffic count
        if (parseInt(csv[row]['Prediction']) > 0){
            if(csv[row]['Date'] in totalViews){
                var value = totalViews[csv[row]['Date']] + parseInt(csv[row]['Prediction'])
                totalViews[csv[row]['Date']] = value;
            }else{
                totalViews[csv[row]['Date']] = parseInt(csv[row]['Prediction'])
            }
        }

        //views per page
        if (parseInt(csv[row]['Prediction']) > 0){
            if(csv[row]['Page_Title'] in pageViews){
                var value = pageViews[csv[row]['Page_Title']] + parseInt(csv[row]['Prediction'])
                pageViews[csv[row]['Page_Title']] = value;
            }else{
                pageViews[csv[row]['Page_Title']] = parseInt(csv[row]['Prediction'])
            }
        }

        //country totals
        if (parseInt(csv[row]['Prediction']) > 0){
            if(csv[row]['Language'] in countryViews){
                var value = countryViews[csv[row]['Language']] + parseInt(csv[row]['Prediction'])
                countryViews[csv[row]['Language']] = value;
            }else{
                countryViews[csv[row]['Language']] = parseInt(csv[row]['Prediction'])
            }
        }
    }
    console.log(totalViews)
    console.log(countryViews)
    var items = Object.keys(pageViews).map(function(key) {
        return [key, pageViews[key]];
      });
      
      // Sort the array based on the second element
    items.sort(function(first, second) {
    return second[1] - first[1];
    });
      
      // Create a new array with only the first 5 items
    console.log(items.slice(0,10));

}

function writeToCSV(totalViews, pageViews, countryViews){
    csvWriter.writeRecords(totalViews).then(() =>{
        console.log('done')
    }).catch(error=>{
        console.log(error);
    })

}

const data = formatChartData('./data/seven_day_forecasted_visits.csv')