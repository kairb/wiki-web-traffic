import csv
from datetime import datetime
from dateutil.parser import parse

start_Date = datetime.strptime("2019-01-01", '%Y-%m-%d')
end_Date = datetime.strptime("2019-01-08", '%Y-%m-%d')

with open("../../public/data/forecasts.csv", "rb") as f:
    writer=csv.writer(open('queriedData.csv', 'w'), delimiter=',')
    csvreader = csv.reader(f, delimiter=",")
    headers = next(csvreader)
    for row in csvreader:
        #print(type(row[3]))
        curr = datetime.strptime(row[3], '%Y-%m-%d')
        row[3] = curr.isoformat()
        if curr > start_Date and curr < end_Date:
            writer.writerow(row)

