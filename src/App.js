import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Query from './components/Query';
import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import './components/Graph'
import Overview from './components/Overview';
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/navbar";

export default function App() {
  return (
  <div className="App">
    <Router>
      <div>
        <Navbar />
      </div>
      <Route path="/" exact component={Overview} />
      <Route path="/overview" component={Overview} />
      <Route path="/query" component={Query} />
    </Router>
  </div>
  );
} 
